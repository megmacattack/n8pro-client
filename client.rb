require 'rubyserial'
require 'hexdump'
require 'pp'

ADDR_PRG = 0x0000000
ADDR_CHR = 0x0800000
ADDR_SRM = 0x1000000
ADDR_CFG = 0x1800000
ADDR_SSR = 0x1802000
ADDR_FIFO = 0x1810000
ADDR_FLA_MENU = 0x00000 #boot fails mos6502 code
ADDR_FLA_FPGA = 0x40000 #boot fails fpga code
ADDR_FLA_ICOR = 0x80000 #mcu firmware update


SIZE_PRG = 0x800000
SIZE_CHR = 0x800000
SIZE_SRM = 0x40000

ADDR_OS_PRG = (ADDR_PRG + 0x7E0000)
ADDR_OS_CHR = (ADDR_CHR + 0x7E0000)

FAT_READ = 0x01
FAT_WRITE = 0x02
FAT_OPEN_EXISTING = 0x00
FAT_CREATE_NEW = 0x04
FAT_CREATE_ALWAYS = 0x08
FAT_OPEN_ALWAYS = 0x10
FAT_OPEN_APPEND = 0x30

CMD_STATUS = 0x10
CMD_GET_MODE = 0x11
CMD_HARD_RESET = 0x12
CMD_GET_VDC = 0x13
CMD_RTC_GET = 0x14
CMD_RTC_SET = 0x15
CMD_FLA_RD = 0x16
CMD_FLA_WR = 0x17
CMD_FLA_WR_SDC = 0x18
CMD_MEM_RD = 0x19
CMD_MEM_WR = 0x1A
CMD_MEM_SET = 0x1B
CMD_MEM_TST = 0x1C
CMD_MEM_CRC = 0x1D
CMD_FPG_USB = 0x1E
CMD_FPG_SDC = 0x1F
CMD_FPG_FLA = 0x20
CMD_FPG_CFG = 0x21
CMD_USB_WR = 0x22
CMD_FIFO_WR = 0x23
CMD_UART_WR = 0x24
CMD_REINIT = 0x25
CMD_SYS_INF = 0x26
CMD_GAME_CTR = 0x27
CMD_UPD_EXEC = 0x28


CMD_DISK_INIT = 0xC0
CMD_DISK_RD = 0xC1
CMD_DISK_WR = 0xC2
CMD_F_DIR_OPN = 0xC3
CMD_F_DIR_RD = 0xC4
CMD_F_DIR_LD = 0xC5
CMD_F_DIR_SIZE = 0xC6
CMD_F_DIR_PATH = 0xC7
CMD_F_DIR_GET = 0xC8
CMD_F_FOPN = 0xC9
CMD_F_FRD = 0xCA
CMD_F_FRD_MEM = 0xCB
CMD_F_FWR = 0xCC
CMD_F_FWR_MEM = 0xCD
CMD_F_FCLOSE = 0xCE
CMD_F_FPTR = 0xCF
CMD_F_FINFO = 0xD0
CMD_F_FCRC = 0xD1
CMD_F_DIR_MK = 0xD2
CMD_F_DEL = 0xD3


CMD_USB_RECOV = 0xF0;
CMD_RUN_APP = 0xF1;

PLUS = '+'.unpack("C").pop

def write_data(port, data)
	while data.length > 0
		puts '---'
		pp [data]
		written = port.write(data)
		data = data[written..-1]
		pp [written, data]
		puts '==='
	end
end

def write_cmd(port, cmd_num, *extra)
	str = [PLUS, PLUS ^ 0xff, cmd_num, cmd_num ^ 0xff].pack('CCCC')
	pp str
	write_data(port, str)
	extra.each do |blob|
		pp blob.unpack("C*")
		write_data(port, blob)
	end
end	

def read_bytes(port, count)
	bytes = ""
	while count > 0
		sleep(0.1)
		new_bytes = port.read(count)
		if new_bytes.length > 0
			bytes += new_bytes
			print '.'*new_bytes.length
			STDOUT.flush
			count -= new_bytes.length
		end
	end
	puts
	bytes
end

def get_status(port)
	write_cmd(port, CMD_STATUS)
	status = read_bytes(port, 2).unpack("S").pop
	if status & 0xff00 != 0xa500
		raise "invalid status: #{status}"
	else
		status & 0xff
	end
end

def mem_read(port, addr, len)
	write_cmd(port, CMD_MEM_RD,
		[addr, len, 0].pack("LLC"))
	b = read_bytes(port, len)
	get_status(port)
	b
end

def mem_write(port, addr, data)
	write_cmd(port, CMD_MEM_WR,
		[addr, data.length, 0, *data].pack('LLC*'))
	#get_status(port)
end

def code_inject(port, iseq)
	mem_write(port, ADDR_FIFO, [iseq.length, *iseq])
end

port = Serial.new '/dev/ttyACM0'

# read any garbage off the line
#while port.read(1024).length > 0
#	print 'x'
#end
#puts 

pp get_status(port)
#pp mem_read(port, 0x0700, 1)
# save file 1 data
#Hexdump.dump(mem_read(port, ADDR_SRM + 0x7400 - 0x6000, 256))
# current sidescroll data
#Hexdump.dump(mem_read(port, ADDR_SRM + 0, 1024))
# player name 1
#Hexdump.dump(mem_read(port, ADDR_PRG + 0xfa0, 8))

#loop do
#	pp read_bytes(port, 3)
#end

# write instructions to set the sword value to 1
code_inject(port, [
	0xa9, 0x01, # lda #$1
	0x8d, 0x57, 0x06, # sta $0657
	0x60, # rts
])