# Zelda 2 Famicom Expansion Port Controller Support

The patches in this directory add support for the original famicom's
expansion port's ability to connect a controller (either third party,
or a US controller via adapter).

Zelda 2 is one of the rare first party games that doesn't support this.
Looking around the only other example I could find was SMB2US, which
it seems like there's some other evidence of the two games sharing
some engine code.
