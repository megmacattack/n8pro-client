#org $f5
#var controller1Data, 1
#var controller2Data, 1

; Controller 1 read -- hook to jsr to nearby unused space
#org $d375, $1d385, 3
; original (from trax disassembly)
;   lsr                                    ; 0x1d385 $D375 4A                      ;
;   rol      $F5                           ; 0x1d386 $D376 26 F5                   ;; Controller 1 Buttons Pressed
jsr controller1Read

; Replacement controller register parse - reads bottom low bits (D0 and D1)
; and uses that, instead of just the bottom bit (D0). On the famicom, controllers
; plugged in to the ext port use D1.
#org $d39a, $1d3aa, 48
controller1Read:
; from nesdev wiki - https://wiki.nesdev.org/w/index.php?title=Controller_reading_code
	and #%00000011  ; ignore bits other than controller
	cmp #$01        ; Set carry if and only if nonzero
	rol controller1Data ; rotate carry bit into controller data value
	rts

