;;;
;;; Register addresses
;;;
#org $40f0
#var FIFO_DATA, 1
#var FIFO_STATUS, 1

;;; 
;;; Variables in zero page to make code size small
;;;
#org $cc ; cc-d2 appears to be unused? Should probably verify somehow.
#var payloadOffset, 1
#var payloadNewValue, 1
#var payloadOldValue, 1
#var payloadSize, 1

;;;
;;; Well known and useful locations of game state
;;;
#org $0657
itemStateTable:

;;;
;;; Bottom of stack area, used as scratch area to put code into from usb communications.
;;;
#org $0110 ; $0100-010f is technically part of stack but appears to be used by game code a bunch
codeScratchArea:

;;;
;;; Wherever we do a write to the itemStateTable, we need to inject a jsr sendItemStateChange. This will both
;;; do the intended instruction (`sta itemStateTable, Y`) but also forward a message over the fifo
;;; about what value was changed from what to what.
;;;
; instruction pointer differs drastically from file offset because this code is copied into sram at load time (because fds port)
#org $748B, $6D0B, 3
itemGetInject:
	jsr sendItemStateChange

;;;
;;; This is a function that sends a message to the fifo about what was changed and from what and to what that can be jsr'd
;;; to from a sta indirect instruction.
;;;
;#org $ffc0, $1ffd0, 40
#org $b751, $7761, $2ef
sendItemStateChange:
	ldx itemStateTable, Y ; save old value
	sta itemStateTable, Y ; do actual thing (store new value to item location)
	sty payloadOffset ; store values in comm array
	stx payloadOldValue
	sta payloadNewValue
	ldx #0
	.headerLoop:
	lda sendItemStateChangeMsgHeader, X
	sta FIFO_DATA
	inx
	cpx #(sendItemStateChangeMsgHeaderEnd - sendItemStateChangeMsgHeader)
	bne .headerLoop
	; don't need to do this, we never write to y: ldy payloadOffset
	sty FIFO_DATA
	ldx payloadOldValue
	stx FIFO_DATA
	lda payloadNewValue
	sta FIFO_DATA
	rts

;;;
;;; The usb write message header. This never changes, so it can just sit in rom and be copied.
;;;
#org $ed94, $1eda4, $C
sendItemStateChangeMsgHeader:
; n8pro protocol header, '+', '+'^0xff, CMD_USB_WR, CMD_USB_WR^0xff, len in LE BO
#byte '+', $2b ^ $ff, $22, $22 ^ $ff
#byte 4, 0
; our header, 'i' for 'item'.
#byte 'i'
sendItemStateChangeMsgHeaderEnd:

;;; address of the original nmi hook function
#org $E484
originalNMI:

;;;
;;; nmi hook function to check for code dump from usb interface, loads code into ram and jumps to it
;;; it's up to the host to include code to signal that it has successfully handled the data
;;; by writing a completion message to the fifo.
;;;
#org $ffc0, $1ffd0, 40
usbDataHook:
	lda FIFO_STATUS
	bmi .done
	cmp #$40 ; extra check for special value mesen returns for unknown registers so we don't break everything
	beq .done
	lda FIFO_DATA
	sta payloadSize
	ldx #$0
	.loop:
	cpx payloadSize
	beq .callPayload
	jsr fifoRead
	inx
	jmp .loop
	.callPayload:
	jsr codeScratchArea
	.done:
	jmp originalNMI

#org $ff43, $1ff53, $D
fifoRead: ; reads from fifo to codeScratchArea[X]
	.waitByteLoop:
	lda FIFO_STATUS
	bmi .waitByteLoop
	lda FIFO_DATA
	sta codeScratchArea,X
	rts


;;;
;;; rewrite nmi pointer to be the above function.
;;;
#org $fffa, $2000a, 2
nmiPointer:
#word usbDataHook